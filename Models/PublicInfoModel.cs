﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Widgets.CartLifeTimer.Models
{
    public class PublicInfoModel : BaseNopModel
    {
        public int SecRemaining { get; set; }
        public string ClearUrl { get; set; }
    }
}
