﻿using System.Collections.Generic;

namespace Nop.Plugin.Widgets.CartLifeTimer.Models.Requests
{
    public class Cliente
    {
        public string Nombre { get; set; }

        public string ApellidoPat { get; set; }

        public string ApellidoMat { get; set; }

        public Domicilio Domicilio { get; set; }

        public List<Telefono> Telefono { get; set; }
    }
}