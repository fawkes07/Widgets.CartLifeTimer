﻿namespace Nop.Plugin.Widgets.CartLifeTimer.Models.Requests
{
    public enum TipoTelefono
    {
        Movil = 1,
        Casa = 2
    }

    public class Telefono
    {
        public TipoTelefono Tipo { get; set; }
        public string Numero { get; set; }
    }
}