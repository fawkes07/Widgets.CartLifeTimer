﻿using System.Collections.Generic;
using Nop.Plugin.Widgets.CartLifeTimer.Utils.http;

namespace Nop.Plugin.Widgets.CartLifeTimer.Models.Requests
{
    public class DetPedido : ICommObject
    {
        /// <summary>
        /// 
        /// </summary>
        public int Cantidad { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ComboId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Producto> ProductoCollection { get; set; }
    }
}
