﻿namespace Nop.Plugin.Widgets.CartLifeTimer.Models.Requests
{
    public class Pago
    {
        public decimal Monto { get; set; }
        public decimal Comision { get; set; }
        public string Fecha { get; set; }
        public string TransaccionId { get; set; }
        public string ValeId { get; set; }
    }
}