﻿using Nop.Plugin.Widgets.CartLifeTimer.Utils.http;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.CartLifeTimer.Models.Requests
{
    public enum eEstatusId
    {
        Cancelar = 2,
        Cerrar = 1
    };

    public class Pedido : ICommObject
    {
        /// <summary>
        /// 
        /// </summary>
        public string PedidoId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PedidoWebId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PedidoNopId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ClienteNopId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Cantidad { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Monto { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Estatus { get; set; }

        /// <summary>
        /// [0] : Cancelar,
        /// [1] : Cerrar
        /// </summary>
        public int EstatusId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FechaEstatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MessageResult { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<DetPedido> DetalleCollection { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FechaVigencia { get; set; }
        
        public Pago Pago { get; set; }
        
        public Cliente Cliente { get; set; }
        
        public string NoGuia { get; set; }
    }
}
