﻿namespace Nop.Plugin.Widgets.CartLifeTimer.Models.Requests
{
    public class Domicilio
    {
        public string Calle { get; set; }
        public int NumExt { get; set; }
        public string NumInt { get; set; }
        public string EntreCalle1 { get; set; }
        public string EntreCalle2 { get; set; }
        public string Referencia { get; set; }
        public int CodigoPostal { get; set; }
        public string Colonia { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
    }
}