﻿using Nop.Plugin.Widgets.CartLifeTimer.Utils.http;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.CartLifeTimer.Models.Requests
{
    /// <summary>
    ///*********************************************************************************
    /// Proyecto: AppCklass
    /// Creacion por: JRAMIREZ
    /// Descripcion: Clase Producto que representa la estructura de un producto a devolver en el Servicio.
    ///*********************************************************************************
    /// </summary>
    public class Producto : ICommObject
    {
        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public string ProductoId { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public decimal PrecioUnitario { get; set; }


        /// <summary>
        /// Descripcion: Combo campo agrupador de producto
        /// </summary>
        public string Combo { get; set; }

        /// <summary>
        /// Descripcion: Modelo del producto
        /// </summary>
        public string Modelo { get; set; }

        /// <summary>
        /// Descripcion: Indica si el producto sólo aplica para kits
        /// </summary>
        public bool SoloKit { get; set; }

        /// <summary>
        /// Descripcion: Material del producto
        /// </summary>
        public string MaterialId { get; set; }

        /// <summary>
        /// Descripcion: Descripcion del producto
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Descripcion: Catalogo al que pertenece 
        /// </summary>
        public string Catalogo { get; set; }

        ///// <summary>
        ///// Descripcion: Tipo a que genero va dirigido el producto
        ///// </summary>
        //[DataMember]
        //public string Tipo { get; set; }

        ///// <summary>
        ///// Descripcion: Temporada del producto
        ///// </summary>
        //[DataMember]
        //public string Temporada { get; set; }

        /// <summary>
        /// Descripcion: Lista de los Colres por producto
        /// </summary>
        public List<Colores> Colores { get; set; }

        /// <summary>
        /// Descripcion: Lista de Tallas por producto
        /// </summary>
        public List<Tallas> Tallas { get; set; }

        ///// <summary>
        ///// Descripcion: Cantidades de existencia por talla
        ///// </summary>
        //[DataMember]
        //public List<Cantidades> Cantidades { get; set; }
        #endregion
    }

    /// <summary>
    ///*********************************************************************************
    /// Proyecto: AppCklass
    /// Creacion por: JRAMIREZ
    /// Descripcion: Clase Info que representa la estructura de la informacion que se obtiene de la sucursal
    ///*********************************************************************************
    /// </summary>
    public class Info
    {
        #region Propiedades
        /// <summary> 
        /// Descripcion: Agrupador de productos de paquete
        /// </summary>
        public string Combo { get; set; }

        /// <summary>
        /// Descripcion: Modelo del producto
        /// </summary>
        public string Modelo { get; set; }

        /// <summary>
        /// Descripcion: MaterialId del producto
        /// </summary>
        public string MaterialId { get; set; }

        /// <summary>
        /// Descripcion: ColorId del producto
        /// </summary>
        public string ColorId { get; set; }

        /// <summary>
        /// Descripcion: Talla del producto
        /// </summary>
        public string Talla { get; set; }

        /// <summary>
        /// Descripcion: ProductoId 
        /// </summary>
        public string ProductoId { get; set; }

        /// <summary>
        /// Descripcion: Color del producto.
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Descripcion: Descripcion del producto
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Descripcion: Catalogo del producto
        /// </summary>
        public string Catalogo { get; set; }

        /// <summary>
        /// Descripcion: Ropa/Zapato
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// Descripcion: Codigo de temporada 12,10,0
        /// </summary>
        public string Temporada { get; set; }

        /// <summary>
        /// Descripcion: Cantidad en existencia
        /// </summary>
        public double Cantidad { get; set; }
        #endregion
    }

    /// <summary>
    ///*********************************************************************************
    /// Proyecto: AppCklass
    /// Creacion por: JRAMIREZ
    /// Descripcion: Clase Colores es la estructura para generar listado de Colores
    ///*********************************************************************************
    /// </summary>
    public class Colores
    {
        #region Propiedades
        /// <summary>
        /// Descripcion: ColorId del color
        /// </summary>
        public string ColorId { get; set; }

        /// <summary>
        /// Descripcion: Descripcion del color
        /// </summary>
        public string Color { get; set; }
        #endregion
    }

    /// <summary>
    ///*********************************************************************************
    /// Proyecto: AppCklass
    /// Creacion por: JRAMIREZ
    /// Descripcion: Clase Tallas que representa la estructura de un talla a devolver en el Servicio.
    ///*********************************************************************************
    /// </summary>
    public class Tallas
    {
        #region Propiedades
        /// <summary>
        /// Descripcion: Talla del producto
        /// </summary>
        public string Talla { get; set; }
        #endregion
    }

    /// <summary>
    ///*********************************************************************************
    /// Proyecto: AppCklass
    /// Creacion por: JRAMIREZ
    /// Descripcion: Clase Cantidades que representa la estructura de un producto y su cantidad a devolver en el Servicio.
    ///*********************************************************************************
    /// </summary>
    public class Cantidades
    {
        #region Propiedades
        /// <summary>
        /// Descripcion: ProductoId
        /// </summary>
        public string ProductoId { get; set; }

        /// <summary>
        /// Descripcion: Existencia del producto
        /// </summary>
        public double Cantidad { get; set; }
        #endregion
    }
}