﻿using System.Collections.Generic;

namespace Nop.Plugin.Widgets.CartLifeTimer.Models.Requests
{
    /// <summary>
    /// 
    /// </summary>
    public class RequestCambiarEstatus
    {
        /// <summary>
        /// 
        /// </summary>
        public List<PropCambiarEstatus> PropCambiarEstatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int EstatusId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PropCambiarEstatus
    {
        /// <summary>
        /// 
        /// </summary>
        public int ClienteNopId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PedidoNopId { get; set; }
    }
}
