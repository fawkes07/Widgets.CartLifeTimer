﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.CartLifeTimer.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public ConfigurationModel()
        {
            AvailableZones = new List<SelectListItem>();
        }

        public int ActiveStoreScopeConfiguration { get; set; }
        
        [NopResourceDisplayName("Admin.ContentManagement.Widgets.ChooseZone")]
        public string ZoneId { get; set; }
        public IList<SelectListItem> AvailableZones { get; set; }
        public bool ZoneId_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CartLifeTimer.URL_CkalssWS")]
        public string URL_CkalssWS { get; set; }
        public bool URL_CkalssWS_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CartLifeTimer.CartLifeTime")]
        public int CartLifeTime { get; set; }
        public bool CartLifeTime_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CartLifeTimer.GenDebugLog")]
        public bool GenDebugLog { get; set; }
        public bool GenDebugLog_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CartLifeTimer.UseSandbox")]
        public bool UseSandbox { get; set; }
        public bool UseSandbox_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CartLifeTimer.URL_ClearCart")]
        public string URL_ClearCart { get; set; }
        public bool URL_ClearCart_OverrideForStore { get; set; }
    }
}
