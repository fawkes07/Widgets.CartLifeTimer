﻿using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Plugin.Widgets.CartLifeTimer.Models.Requests;
using Nop.Plugin.Widgets.CartLifeTimer.Utils.http;
using Nop.Plugin.Widgets.CartLifeTimer.WebServices.Client;
using Nop.Services.Logging;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.CartLifeTimer.Utils
{
    public class CklCancelCart
    {
        public static Pedido CancelOrderByNopCustomer(Customer customer)
        {
            var _pluginSettins = EngineContext.Current.Resolve<CartLifeTimerSettings>();
            var _workContext = EngineContext.Current.Resolve<IWorkContext>();
            var _logger = EngineContext.Current.Resolve<ILogger>();

            var serv = new CklClienteCancelarPedido(_pluginSettins.URL_CkalssWS);
            var responser = new Pedido();

            try
            {
                responser = serv.CambiarEstatus(customer.Id, (int)eEstatusId.Cancelar);
            }
            catch (Exception ex)
            {
                _logger.Error("CklCancelCart > CancelOrderByNopCustomer > " + ex.Message, ex, _workContext.CurrentCustomer);
            }

            if (_pluginSettins.GenDebugLog)
                _logger.Information("Se cancelara el pedido del usuario con id: " + customer.Id);

            return responser;
        }

        public static List<Pedido> CancelOrdersByNopCustomers(List<int> CustomersIds)
        {
            var _SynckCklSettings = EngineContext.Current.Resolve<CartLifeTimerSettings>();
            var _workContext = EngineContext.Current.Resolve<IWorkContext>();
            var _logger = EngineContext.Current.Resolve<ILogger>();

            List<Pedido> resultados = new List<Pedido>();

            RequestCambiarEstatus RequestData = new RequestCambiarEstatus();
            RequestData.PropCambiarEstatus = new List<PropCambiarEstatus>();

            foreach (var CustomerID in CustomersIds)
            {
                RequestData.PropCambiarEstatus.Add(new PropCambiarEstatus
                {
                    ClienteNopId = CustomerID
                });
            }

            RequestData.EstatusId = (int)eEstatusId.Cancelar;

            try
            {
                resultados = HttpUtils.Request<List<Pedido>>(_SynckCklSettings.URL_CkalssWS + "CambiarEstatusPedido", HttpMethod.POST, RequestData);
            }
            catch (Exception ex)
            {
                _logger.Error("SyncCklProduct > CancelOrdersByNopCustomers > " + ex.Message, ex, _workContext.CurrentCustomer);
            }
            return resultados;
        }
    }
}
