﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Widgets.CartLifeTimer.Models;
using Nop.Plugin.Widgets.CartLifeTimer.Models.Requests;
using Nop.Plugin.Widgets.CartLifeTimer.Utils;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.CartLifeTimer.Controllers
{
    public class WidgetsCartLifeTimerController : BasePluginController
    {
        #region Services
        
        private readonly ILogger _logger;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IShoppingCartService _shoppingCartService;

        #endregion

        #region Ctor

        public WidgetsCartLifeTimerController(
            ILogger logger,
            IWorkContext workContext,
            IStoreContext storeContext,
            IStoreService storeService,
            ISettingService settingService,
            ILocalizationService localizationService,
            IShoppingCartService shoppingCartService
            )
        {
            this._logger = logger;
            this._workContext = workContext;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._shoppingCartService = shoppingCartService;
        }

        #endregion

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var _cartLineTimerSettings = _settingService.LoadSetting<CartLifeTimerSettings>(storeScope);

            var model = new ConfigurationModel
            {
                ZoneId = _cartLineTimerSettings.WidgetZone,
                URL_CkalssWS = _cartLineTimerSettings.URL_CkalssWS,
                CartLifeTime = _cartLineTimerSettings.CartLifeTime,
                GenDebugLog = _cartLineTimerSettings.GenDebugLog,
                UseSandbox = _cartLineTimerSettings.UseSandbox,
                URL_ClearCart = _cartLineTimerSettings.URL_ClearCart
            };

            #region fil list regions

            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_blog_before", Value = "left_side_column_blog_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_after_blog_archive", Value = "left_side_column_after_blog_archive" });
            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_blog_after", Value = "left_side_column_blog_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "blogpost_page_top", Value = "blogpost_page_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "blogpost_page_before_body", Value = "blogpost_page_before_body" });
            model.AvailableZones.Add(new SelectListItem() { Text = "blogpost_page_before_comments", Value = "blogpost_page_before_comments" });
            model.AvailableZones.Add(new SelectListItem() { Text = "blogpost_page_inside_comment", Value = "blogpost_page_inside_comment" });
            model.AvailableZones.Add(new SelectListItem() { Text = "blogpost_page_after_comments", Value = "blogpost_page_after_comments" });
            model.AvailableZones.Add(new SelectListItem() { Text = "blogpost_page_bottom", Value = "blogpost_page_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_blog_before", Value = "left_side_column_blog_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_after_blog_archive", Value = "left_side_column_after_blog_archive" });
            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_blog_after", Value = "left_side_column_blog_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "bloglist_page_before_posts", Value = "bloglist_page_before_posts" });
            model.AvailableZones.Add(new SelectListItem() { Text = "bloglist_page_before_post", Value = "bloglist_page_before_post" });
            model.AvailableZones.Add(new SelectListItem() { Text = "bloglist_page_before_post_body", Value = "bloglist_page_before_post_body" });
            model.AvailableZones.Add(new SelectListItem() { Text = "bloglist_page_after_post_body", Value = "bloglist_page_after_post_body" });
            model.AvailableZones.Add(new SelectListItem() { Text = "bloglist_page_inside_post", Value = "bloglist_page_inside_post" });
            model.AvailableZones.Add(new SelectListItem() { Text = "bloglist_page_after_post", Value = "bloglist_page_after_post" });
            model.AvailableZones.Add(new SelectListItem() { Text = "bloglist_page_after_posts", Value = "bloglist_page_after_posts" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_activediscussions_after_header", Value = "boards_activediscussions_after_header" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_activediscussions_after_topics", Value = "boards_activediscussions_after_topics" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_forum_after_header", Value = "boards_forum_after_header" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_forum_top", Value = "boards_forum_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_forum_bottom", Value = "boards_forum_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_forumgroup_after_header", Value = "boards_forumgroup_after_header" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_forumgroup_bottom", Value = "boards_forumgroup_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_main_after_header", Value = "boards_main_after_header" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_main_before_activediscussions", Value = "boards_main_before_activediscussions" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_main_after_activediscussions", Value = "boards_main_after_activediscussions" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_postcreate_before", Value = "boards_postcreate_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_postcreate_after", Value = "boards_postcreate_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_postedit_before", Value = "boards_postedit_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_postedit_after", Value = "boards_postedit_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_search_before_searchform", Value = "boards_search_before_searchform" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_search_after_searchform", Value = "boards_search_after_searchform" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_search_before_results", Value = "boards_search_before_results" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_search_after_results", Value = "boards_search_after_results" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_topic_after_header", Value = "boards_topic_after_header" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_topic_top", Value = "boards_topic_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_topic_bottom", Value = "boards_topic_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_topiccreate_before", Value = "boards_topiccreate_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_topiccreate_after", Value = "boards_topiccreate_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_topicedit_before", Value = "boards_topicedit_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "boards_topicedit_after", Value = "boards_topicedit_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "categorydetails_after_breadcrumb", Value = "categorydetails_after_breadcrumb" });
            model.AvailableZones.Add(new SelectListItem() { Text = "categorydetails_top", Value = "categorydetails_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "categorydetails_before_subcategories", Value = "categorydetails_before_subcategories" });
            model.AvailableZones.Add(new SelectListItem() { Text = "categorydetails_before_featured_products", Value = "categorydetails_before_featured_products" });
            model.AvailableZones.Add(new SelectListItem() { Text = "categorydetails_after_featured_products", Value = "categorydetails_after_featured_products" });
            model.AvailableZones.Add(new SelectListItem() { Text = "categorydetails_before_filters", Value = "categorydetails_before_filters" });
            model.AvailableZones.Add(new SelectListItem() { Text = "categorydetails_before_product_list", Value = "categorydetails_before_product_list" });
            model.AvailableZones.Add(new SelectListItem() { Text = "categorydetails_bottom", Value = "categorydetails_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "manufacturerdetails_top", Value = "manufacturerdetails_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "manufacturerdetails_before_featured_products", Value = "manufacturerdetails_before_featured_products" });
            model.AvailableZones.Add(new SelectListItem() { Text = "manufacturerdetails_after_featured_products", Value = "manufacturerdetails_after_featured_products" });
            model.AvailableZones.Add(new SelectListItem() { Text = "manufacturerdetails_before_filters", Value = "manufacturerdetails_before_filters" });
            model.AvailableZones.Add(new SelectListItem() { Text = "manufacturerdetails_before_product_list", Value = "manufacturerdetails_before_product_list" });
            model.AvailableZones.Add(new SelectListItem() { Text = "manufacturerdetails_bottom", Value = "manufacturerdetails_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productsbytag_top", Value = "productsbytag_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productsbytag_before_product_list", Value = "productsbytag_before_product_list" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productsbytag_bottom", Value = "productsbytag_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productsearch_page_basic", Value = "productsearch_page_basic" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productsearch_page_advanced", Value = "productsearch_page_advanced" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productsearch_page_before_results", Value = "productsearch_page_before_results" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productsearch_page_after_results", Value = "productsearch_page_after_results" });
            model.AvailableZones.Add(new SelectListItem() { Text = "searchbox_before_search_button", Value = "searchbox_before_search_button" });
            model.AvailableZones.Add(new SelectListItem() { Text = "searchbox", Value = "searchbox" });
            model.AvailableZones.Add(new SelectListItem() { Text = "header_menu_before", Value = "header_menu_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "header_menu_after", Value = "header_menu_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "mob_header_menu_before", Value = "mob_header_menu_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "mob_header_menu_after", Value = "mob_header_menu_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "vendordetails_top", Value = "vendordetails_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "vendordetails_bottom", Value = "vendordetails_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_billing_address_top", Value = "checkout_billing_address_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_billing_address_middle", Value = "checkout_billing_address_middle" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_billing_address_bottom", Value = "checkout_billing_address_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_progress_before", Value = "checkout_progress_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_progress_after", Value = "checkout_progress_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_completed_top", Value = "checkout_completed_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_completed_bottom", Value = "checkout_completed_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_confirm_top", Value = "checkout_confirm_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_confirm_bottom", Value = "checkout_confirm_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "opc_content_before", Value = "opc_content_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "opc_content_after", Value = "opc_content_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_billing_address_top", Value = "op_checkout_billing_address_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_billing_address_middle", Value = "op_checkout_billing_address_middle" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_billing_address_bottom", Value = "op_checkout_billing_address_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_confirm_top", Value = "op_checkout_confirm_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_confirm_bottom", Value = "op_checkout_confirm_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_payment_info_top", Value = "op_checkout_payment_info_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_payment_info_bottom", Value = "op_checkout_payment_info_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_payment_method_top", Value = "op_checkout_payment_method_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_payment_method_bottom", Value = "op_checkout_payment_method_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_shipping_address_top", Value = "op_checkout_shipping_address_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_shipping_address_middle", Value = "op_checkout_shipping_address_middle" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_shipping_address_bottom", Value = "op_checkout_shipping_address_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_shipping_method_top", Value = "op_checkout_shipping_method_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "op_checkout_shipping_method_bottom", Value = "op_checkout_shipping_method_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_payment_info_top", Value = "checkout_payment_info_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_payment_info_bottom", Value = "checkout_payment_info_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_payment_method_top", Value = "checkout_payment_method_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_payment_method_bottom", Value = "checkout_payment_method_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_shipping_address_top", Value = "checkout_shipping_address_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_shipping_address_middle", Value = "checkout_shipping_address_middle" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_shipping_address_bottom", Value = "checkout_shipping_address_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_shipping_method_top", Value = "checkout_shipping_method_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "checkout_shipping_method_bottom", Value = "checkout_shipping_method_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "contactus_top", Value = "contactus_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "contactus_bottom", Value = "contactus_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "contactvendor_top", Value = "contactvendor_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "contactvendor_bottom", Value = "contactvendor_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "footer", Value = "footer" });
            model.AvailableZones.Add(new SelectListItem() { Text = "header_links_before", Value = "header_links_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "header_links_after", Value = "header_links_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "sitemap_before", Value = "sitemap_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "sitemap_after", Value = "sitemap_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "account_navigation_before", Value = "account_navigation_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "account_navigation_after", Value = "account_navigation_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "home_page_top", Value = "home_page_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "home_page_before_categories", Value = "home_page_before_categories" });
            model.AvailableZones.Add(new SelectListItem() { Text = "home_page_before_products", Value = "home_page_before_products" });
            model.AvailableZones.Add(new SelectListItem() { Text = "home_page_before_best_sellers", Value = "home_page_before_best_sellers" });
            model.AvailableZones.Add(new SelectListItem() { Text = "home_page_before_news", Value = "home_page_before_news" });
            model.AvailableZones.Add(new SelectListItem() { Text = "home_page_before_poll", Value = "home_page_before_poll" });
            model.AvailableZones.Add(new SelectListItem() { Text = "home_page_bottom", Value = "home_page_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "newslist_page_before_items", Value = "newslist_page_before_items" });
            model.AvailableZones.Add(new SelectListItem() { Text = "newslist_page_inside_item", Value = "newslist_page_inside_item" });
            model.AvailableZones.Add(new SelectListItem() { Text = "newslist_page_after_items", Value = "newslist_page_after_items" });
            model.AvailableZones.Add(new SelectListItem() { Text = "newsitem_page_before_body", Value = "newsitem_page_before_body" });
            model.AvailableZones.Add(new SelectListItem() { Text = "newsitem_page_before_comments", Value = "newsitem_page_before_comments" });
            model.AvailableZones.Add(new SelectListItem() { Text = "newsitem_page_inside_comment", Value = "newsitem_page_inside_comment" });
            model.AvailableZones.Add(new SelectListItem() { Text = "newsitem_page_after_comments", Value = "newsitem_page_after_comments" });
            model.AvailableZones.Add(new SelectListItem() { Text = "orderdetails_page_top", Value = "orderdetails_page_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "orderdetails_page_overview", Value = "orderdetails_page_overview" });
            model.AvailableZones.Add(new SelectListItem() { Text = "orderdetails_page_beforeproducts", Value = "orderdetails_page_beforeproducts" });
            model.AvailableZones.Add(new SelectListItem() { Text = "orderdetails_product_line", Value = "orderdetails_product_line" });
            model.AvailableZones.Add(new SelectListItem() { Text = "orderdetails_page_afterproducts", Value = "orderdetails_page_afterproducts" });
            model.AvailableZones.Add(new SelectListItem() { Text = "orderdetails_page_bottom", Value = "orderdetails_page_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_add_info", Value = "productdetails_add_info" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productbreadcrumb_before", Value = "productbreadcrumb_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productbreadcrumb_after", Value = "productbreadcrumb_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productreviews_page_top", Value = "productreviews_page_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productreviews_page_inside_review", Value = "productreviews_page_inside_review" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productreviews_page_bottom", Value = "productreviews_page_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_after_breadcrumb", Value = "productdetails_after_breadcrumb" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_top", Value = "productdetails_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_before_pictures", Value = "productdetails_before_pictures" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_after_pictures", Value = "productdetails_after_pictures" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_overview_top", Value = "productdetails_overview_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_inside_overview_buttons_before", Value = "productdetails_inside_overview_buttons_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_inside_overview_buttons_after", Value = "productdetails_inside_overview_buttons_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_overview_bottom", Value = "productdetails_overview_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_before_collateral", Value = "productdetails_before_collateral" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_bottom", Value = "productdetails_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_after_breadcrumb", Value = "productdetails_after_breadcrumb" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_top", Value = "productdetails_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_before_pictures", Value = "productdetails_before_pictures" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_after_pictures", Value = "productdetails_after_pictures" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_overview_top", Value = "productdetails_overview_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_inside_overview_buttons_before", Value = "productdetails_inside_overview_buttons_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_inside_overview_buttons_after", Value = "productdetails_inside_overview_buttons_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_overview_bottom", Value = "productdetails_overview_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_before_collateral", Value = "productdetails_before_collateral" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productdetails_bottom", Value = "productdetails_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "profile_page_info_userdetails", Value = "profile_page_info_userdetails" });
            model.AvailableZones.Add(new SelectListItem() { Text = "profile_page_info_userstats", Value = "profile_page_info_userstats" });
            model.AvailableZones.Add(new SelectListItem() { Text = "main_column_before", Value = "main_column_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "main_column_after", Value = "main_column_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_before", Value = "left_side_column_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_after_category_navigation", Value = "left_side_column_after_category_navigation" });
            model.AvailableZones.Add(new SelectListItem() { Text = "left_side_column_after", Value = "left_side_column_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "main_column_before", Value = "main_column_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "main_column_after", Value = "main_column_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "notifications", Value = "notifications" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productbox_addinfo_before", Value = "productbox_addinfo_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productbox_addinfo_middle", Value = "productbox_addinfo_middle" });
            model.AvailableZones.Add(new SelectListItem() { Text = "productbox_addinfo_after", Value = "productbox_addinfo_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "body_start_html_tag_after", Value = "body_start_html_tag_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "content_before", Value = "content_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "content_after", Value = "content_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "body_end_html_tag_before", Value = "body_end_html_tag_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "head_html_tag", Value = "head_html_tag" });
            model.AvailableZones.Add(new SelectListItem() { Text = "header", Value = "header" });
            model.AvailableZones.Add(new SelectListItem() { Text = "header_selectors", Value = "header_selectors" });
            model.AvailableZones.Add(new SelectListItem() { Text = "order_summary_content_before", Value = "order_summary_content_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "order_summary_cart_footer", Value = "order_summary_cart_footer" });
            model.AvailableZones.Add(new SelectListItem() { Text = "order_summary_content_deals", Value = "order_summary_content_deals" });
            model.AvailableZones.Add(new SelectListItem() { Text = "order_summary_content_after", Value = "order_summary_content_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "applyvendor_top", Value = "applyvendor_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "applyvendor_bottom", Value = "applyvendor_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_dashboard_top", Value = "admin_dashboard_top" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_dashboard_news_after", Value = "admin_dashboard_news_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_dashboard_commonstatistics_after", Value = "admin_dashboard_commonstatistics_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_dashboard_customerordercharts_after", Value = "admin_dashboard_customerordercharts_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_dashboard_orderreports_after", Value = "admin_dashboard_orderreports_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_dashboard_latestorders_searchterms_after", Value = "admin_dashboard_latestorders_searchterms_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_dashboard_bottom", Value = "admin_dashboard_bottom" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_header_before", Value = "admin_header_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_header_toggle_after", Value = "admin_header_toggle_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_header_navbar_before", Value = "admin_header_navbar_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_header_middle", Value = "admin_header_middle" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_header_navbar_after", Value = "admin_header_navbar_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_header_after", Value = "admin_header_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_searchbox_before", Value = "admin_searchbox_before" });
            model.AvailableZones.Add(new SelectListItem() { Text = "admin_menu_before", Value = "admin_menu_before" });

            /*
            model.AvailableZones.Add(new SelectListItem() { Text = "Despues del menu cabecera", Value = "header_menu_after" });
            model.AvailableZones.Add(new SelectListItem() { Text = "Head html tag", Value = "head_html_tag" });
            */

            #endregion

            model.ActiveStoreScopeConfiguration = storeScope;

            if (storeScope > 0)
            {
                model.ZoneId_OverrideForStore = _settingService.SettingExists(_cartLineTimerSettings, x => x.WidgetZone, storeScope);
                model.URL_CkalssWS_OverrideForStore = _settingService.SettingExists(_cartLineTimerSettings, x => x.URL_CkalssWS, storeScope);
                model.CartLifeTime_OverrideForStore = _settingService.SettingExists(_cartLineTimerSettings, x => x.CartLifeTime, storeScope);
                model.GenDebugLog_OverrideForStore = _settingService.SettingExists(_cartLineTimerSettings, x => x.GenDebugLog, storeScope);
                model.UseSandbox_OverrideForStore = _settingService.SettingExists(_cartLineTimerSettings, x => x.UseSandbox, storeScope);
                model.URL_ClearCart_OverrideForStore = _settingService.SettingExists(_cartLineTimerSettings, x => x.URL_ClearCart, storeScope);
            }

            return View("~/Plugins/Widgets.CartLifeTimer/Views/WidgetsCartLifeTimer/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var _cartLineTimerSettings = _settingService.LoadSetting<CartLifeTimerSettings>(storeScope);

            //load settings for a chosen store scope
            _cartLineTimerSettings.WidgetZone = model.ZoneId;
            _cartLineTimerSettings.URL_CkalssWS = model.URL_CkalssWS;
            _cartLineTimerSettings.CartLifeTime = model.CartLifeTime;
            _cartLineTimerSettings.GenDebugLog = model.GenDebugLog;
            _cartLineTimerSettings.UseSandbox = model.UseSandbox;
            _cartLineTimerSettings.URL_ClearCart = model.URL_ClearCart;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(_cartLineTimerSettings, x => x.WidgetZone, model.ZoneId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(_cartLineTimerSettings, x => x.URL_CkalssWS, model.URL_CkalssWS_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(_cartLineTimerSettings, x => x.CartLifeTime, model.CartLifeTime_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(_cartLineTimerSettings, x => x.GenDebugLog, model.GenDebugLog_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(_cartLineTimerSettings, x => x.UseSandbox, model.UseSandbox_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(_cartLineTimerSettings, x => x.URL_ClearCart, model.URL_ClearCart_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        [ChildActionOnly]
        public ActionResult PublicInfo(string widgetZone, object additionalData = null)
        {
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var _cartLineTimerSettings = _settingService.LoadSetting<CartLifeTimerSettings>(storeScope);

            var model = new PublicInfoModel();

            model.ClearUrl = _cartLineTimerSettings.URL_ClearCart;

            var lastItem = _workContext.CurrentCustomer.ShoppingCartItems
                                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                                .LimitPerStore(_storeContext.CurrentStore.Id)
                                .ToList().OrderByDescending(x => x.UpdatedOnUtc).FirstOrDefault();

            if(lastItem != null)
            {
                var SegDesdeActualizacion = DateTime.UtcNow.Subtract(lastItem.UpdatedOnUtc).TotalSeconds;
                model.SecRemaining = (_cartLineTimerSettings.CartLifeTime * 60) - (int)SegDesdeActualizacion;

                if (model.SecRemaining <= 0)
                {
                    ClearCart();
                    model.SecRemaining = 0;
                }
            }

            return View("~/Plugins/Widgets.CartLifeTimer/Views/WidgetsCartLifeTimer/PublicInfo.cshtml", model);
        }

        [HttpPost]
        public void ClearCart()
        {
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var _cartLineTimerSettings = _settingService.LoadSetting<CartLifeTimerSettings>(storeScope);

            if (_cartLineTimerSettings.GenDebugLog)
                _logger.Information("ClearCart > Se eliminara el carrito del cliente con ID: " + _workContext.CurrentCustomer.Id);

            var responser = new Pedido();

            responser = _cartLineTimerSettings.UseSandbox ? new Pedido { Success = true } :
                                    CklCancelCart.CancelOrderByNopCustomer(_workContext.CurrentCustomer);

            if (responser.Success)
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();

                cart.ForEach(sci => _shoppingCartService.DeleteShoppingCartItem(sci));
            }
            else
            {
                _logger.Error("ClearCart > No se pudo eliminar el carrito: " + responser.MessageResult);
            }
        }
    }
}