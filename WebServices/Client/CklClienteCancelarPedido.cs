﻿using Nop.Plugin.Widgets.CartLifeTimer.Models.Requests;
using Nop.Plugin.Widgets.CartLifeTimer.Utils.http;
using System;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CartLifeTimer.WebServices.Client
{
    public class CklClienteCancelarPedido : WebApiRequestorBase<Pedido>
    {
        private const string SEGMENT_URL = "CambiarEstatusPedido",
                                ID_FIELD = "";

        public CklClienteCancelarPedido(string baseUrl) : base(baseUrl) { }

        public override Pedido Get(string id = null)
        {
            WebApiRequest req = new WebApiRequest()
            {
                Method = HttpMethod.GET,
                SegmentUrl = SEGMENT_URL + "/Get" + (string.IsNullOrEmpty(id) ? "" : ("?" + ID_FIELD + "=" + id))
            };
            return this.Request(req);
        }

        public override async Task<Pedido> GetAsync(string id = null)
        {
            WebApiRequest req = new WebApiRequest()
            {
                Method = HttpMethod.GET,
                SegmentUrl = SEGMENT_URL + "/Get" + (string.IsNullOrEmpty(id) ? "" : ("?" + ID_FIELD + "=" + id))
            };
            return await this.RequestAsync(req);
        }

        public override Pedido Post(Pedido model)
        {
            WebApiRequest request = new WebApiRequest();
            request.SegmentUrl = SEGMENT_URL;
            request.Method = HttpMethod.POST;
            request.Data = model;
            return this.Request(request);
        }

        public override Task<Pedido> PostAsync(Pedido model)
        {
            WebApiRequest request = new WebApiRequest();
            request.SegmentUrl = SEGMENT_URL;
            request.Method = HttpMethod.POST;
            request.Data = model;
            return this.RequestAsync(request);
        }

        public override Pedido Put(Pedido model)
        {
            throw new NotImplementedException();
        }

        public override Task<Pedido> PutAsync(Pedido model)
        {
            throw new NotImplementedException();
        }

        public override Pedido Delete(string id)
        {
            throw new NotImplementedException();
        }

        public override Task<Pedido> DeleteAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Pedido CambiarEstatus(int ClienteNopId, int EstatusId)
        {
            WebApiRequest req = new WebApiRequest()
            {
                Method = HttpMethod.GET,
                SegmentUrl = SEGMENT_URL + "/" + ClienteNopId + "/" + EstatusId
            };
            return this.Request(req);
        }

    }
}
