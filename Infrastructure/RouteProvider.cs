﻿using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.CartLifeTimer.Infrastructure
{
    class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.Widgets.CartLifeTimer.ClearInvalidCart",
                 "CartLifeTimer/ClearInvalidCart",
                 new { controller = "WidgetsCartLifeTimer", action = "ClearCart" },
                 new[] { "Nop.Plugin.Widgets.CartLifeTimer.Controllers" }
            );
        }

        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
