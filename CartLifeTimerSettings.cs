﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.CartLifeTimer
{
    public class CartLifeTimerSettings : ISettings
    {
        public string WidgetZone { get; set; }

        public string URL_CkalssWS { get; set; }

        public int CartLifeTime { get; set; }

        public bool GenDebugLog { get; set; }

        public bool UseSandbox { get; set; }

        public string URL_ClearCart { get; set; }
    }
}