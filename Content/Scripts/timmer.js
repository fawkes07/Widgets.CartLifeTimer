﻿var timer;

$(document).ready(function () {
    $(window).resize(setTimerPosition);

    if ($(window).width() < 1001) {
        $('#div-timmer').appendTo('.responsive-nav-wrapper');
    }

    if (remainingTime > 0) {
        document.getElementById('div-timmer').style.cssText = 'display:table !important';
        $('#cklass-timer').text('RESTAN ' + formatSeconds(remainingTime));
        timer = setInterval(tick, 1000);
    }
    else {
        document.getElementById('div-timmer').style.cssText = 'display:none !important';
    }
});

function tick() {
    remainingTime--;
    if (remainingTime == 0) {
        clearInterval(timer);
        clearCart();
        document.getElementById('div-timmer').style.cssText = 'display:none !important';
    } else {
        $('#cklass-timer').text('RESTAN ' + formatSeconds(remainingTime));
    }
}

function formatSeconds(seconds) {
    var result = '';
    var min = parseInt(seconds / 60);
    var sec = seconds % 60;
    if (min < 10) {
        result += '0' + min + ':';
    } else {
        result += min + ':';
    }
    if (sec < 10) {
        result += '0' + sec;
    } else {
        result += sec;
    }
    return result;
}

function setTimerPosition() {
    if ($(window).width() < 1001) {
        $('#div-timmer').appendTo('.responsive-nav-wrapper');
    }

    if ($(window).width() > 1000) {
        $('#div-timmer').appendTo('.header-menu');
    }
}