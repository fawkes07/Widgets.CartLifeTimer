﻿using Nop.Core.Domain.Tasks;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Nop.Plugin.Widgets.CartLifeTimer
{
    public class CartLifeTimerPlugin : BasePlugin, IWidgetPlugin
    {
        #region services

        private readonly ISettingService _settingService;
        private readonly IScheduleTaskService _ScheduleTaskService;
        private readonly CartLifeTimerSettings _cartLifeTimerSettings;

        #endregion

        #region Ctor

        public CartLifeTimerPlugin(ISettingService settingService,
            IScheduleTaskService scheduleTaskService,
            CartLifeTimerSettings cartLifeTimerSettings
            )
        {
            this._settingService = settingService;
            this._ScheduleTaskService = scheduleTaskService;
            this._cartLifeTimerSettings = cartLifeTimerSettings;
        }

        #endregion

        #region Gets routes

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "WidgetsCartLifeTimer";
            routeValues = new RouteValueDictionary
                                {
                                    { "Namespaces", "Nop.Plugin.Widgets.CartLifeTimer.Controllers" },
                                    { "area", null }
                                };
        }

        /// <summary>
        /// Gets a route for displaying widget
        /// </summary>
        /// <param name="widgetZone">Widget zone where it's displayed</param>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PublicInfo";
            controllerName = "WidgetsCartLifeTimer";
            routeValues = new RouteValueDictionary
                                {
                                    {"Namespaces", "Nop.Plugin.Widgets.CartLifeTimer.Controllers"},
                                    {"area", null},
                                    {"widgetZone", widgetZone}
                                };
        }

        #endregion

        /// <summary>
        /// Gets widget zones where this widget should be rendered
        /// </summary>
        /// <returns>Widget zones</returns>
        public IList<string> GetWidgetZones()
        {
            return !string.IsNullOrWhiteSpace(_cartLifeTimerSettings.WidgetZone)
                       ? new List<string>() { _cartLifeTimerSettings.WidgetZone }
                       : new List<string>() { "header_selectors" };
        }

        #region override Install & Unistall

        public override void Install()
        {
            _settingService.SaveSetting(new CartLifeTimerSettings
            {
#if DEBUG
                URL_CkalssWS = "https://pedidos.cklass.net/AppDebug/ServicesMov.svc/",
                CartLifeTime = 20,
                GenDebugLog = true,
                WidgetZone = "header_selectors",
                UseSandbox = false,
                URL_ClearCart = "http://<dominio>/CartLifeTimer/ClearInvalidCart/"
#else
                URL_CkalssWS = "https://pedidos.cklass.net/App/ServicesMov.svc/",
                CartLifeTime = 20,
                GenDebugLog = false,
                WidgetZone = "header_selectors",
                UseSandbox = false,
                URL_ClearCart = "https://cklass.com/CartLifeTimer/ClearInvalidCart/"
#endif
            });

            _ScheduleTaskService.InsertTask(new ScheduleTask
            {
                Enabled = true,
                Name = "Servicio de eliminacion de carritos por tiempo",
                Seconds = 60,                   // cada minuto
                StopOnError = false,
                Type = "Nop.Plugin.Widgets.CartLifeTimer.Task.FreeShoppingCartsTask, Nop.Plugin.Widgets.CartLifeTimer"
            });

            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.URL_CkalssWS", "URL Cklass WS");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.URL_CkalssWS.Hint", "Url donde se aloja el webservice cklass");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.CartLifeTime", "Cart life time");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.CartLifeTime.Hint", "Tienpo de vida de los carritos inactivos");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.GenDebugLog", "Generar log de debug");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.GenDebugLog.Hint", "Cuendo se activa se muestra un registro (log) detallado de lo corrimiento");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.UseSandBox", "Activar Sandbox mode");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.UseSandBox.Hint", "ATENCION!! Cuendo se activa el modo Sandbox NO se realizan operaciones servidor de pedidos Cklass");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.URL_ClearCart", "ClearCart UrL");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.CartLifeTimer.URL_ClearCart.Hint", "Esta debe tener el siguiente formato http[s]://<dominio>/CartLifeTimer/ClearInvalidCart");

            base.Install();
        }

        public override void Uninstall()
        {
            _settingService.DeleteSetting<CartLifeTimerSettings>();

            ScheduleTask task = _ScheduleTaskService
                   .GetTaskByType("Nop.Plugin.Widgets.CartLifeTimer.Task.FreeShoppingCartsTask, Nop.Plugin.Widgets.CartLifeTimer");

            if (task != null)
            {
                _ScheduleTaskService.DeleteTask(task);
            }

            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.URL_CkalssWS");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.URL_CkalssWS.Hint");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.CartLifeTime");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.CartLifeTime.Hint");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.GenDebugLog");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.GenDebugLog.Hint");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.UseSandBox");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.UseSandBox.Hint");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.URL_ClearCart");
            this.DeletePluginLocaleResource("Plugins.Widgets.CartLifeTimer.URL_ClearCart.Hint");

            base.Uninstall();
        }

        #endregion
    }
}
